package g30124.BLedea.Marius.L6.ex3;

import java.awt.*;

public  interface  Shape {
    void draw(Graphics g);
    String getId();
}

