package g30124.BLedea.Marius.L6.ex6;

import java.awt.*;

public class Fractals {
    private int x,y,radius;
    private Graphics g;

    public Fractals(int x, int y, int radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getRadius() {
        return radius;
    }

    public void draw(Graphics g, int x, int y, int height, int width) {


        g.drawOval(x+radius,y,radius,radius);
        if(radius >2 )
        {
            radius*=0.75;

        }
        draw(g,x,y,radius,radius);

    }
    public void draw1(Graphics g,int x,int y,int height,int width)
    {
        g.drawOval(x,y,radius,radius);
        if(radius>8)
        {
            //draw1(g,x+radius/2,y,radius,radius);
            draw1(g,x-radius/2,y,radius,radius);
           // draw1(g,x,y+radius/2,radius,radius);
            //draw1(g,x,y-radius/2,radius,radius);
        }
    }


}
