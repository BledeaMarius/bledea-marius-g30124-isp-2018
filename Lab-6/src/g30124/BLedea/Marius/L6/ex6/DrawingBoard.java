package g30124.BLedea.Marius.L6.ex6;

import g30124.BLedea.Marius.L6.ex5.Pyramid;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard extends JFrame {

   Fractals[] fractals=new Fractals[10];


    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(800,800);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    public void addFractals(Fractals fractals1){
        for(int i=0;i<fractals.length;i++){
            if(fractals[i]==null){
                fractals[i] = fractals1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<fractals.length;i++){
            if(fractals[i]!=null)
                fractals[i].draw1(g,fractals[i].getX(),fractals[i].getY(),fractals[i].getRadius(),fractals[i].getRadius());
        }

    }
}
