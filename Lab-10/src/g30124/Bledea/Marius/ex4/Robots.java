package g30124.Bledea.Marius.ex4;

public class Robots extends Thread {
	static Robots [][] matrix=new Robots[100][100];
	int x,y;
	String rname;
	boolean active;

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}


	@Override
	public String toString() {
		return "Robot(" + x + y + ")name='" + rname + '\'' +
				'}';
	}
	Robots (String nume,boolean active,int x,int y)
	{
		this.rname=nume;
		this.active=active;
		this.x=x;
		this.y=y;
		System.out.println("Robot creeat ! Nume="+rname+" ("+this.x+","+this.y+")");
	}

	Robots(String name )
	{	int a,b;
		active=false;

		while(active==false) {
			a = generateNumber();
			b = generateNumber();
			if (checkValid(a, b) == true) {
				active = true;
				this.x = a;
				this.y = b;
				matrix[this.x][this.y]= new Robots(name,active,this.x,this.y);
				matrix[this.x][this.y].start();
			}

		}
	}
	public int generateNumber()
	{
		return (int) (Math.random()*100);
	}
	public void run()
	{
		int old_coord;

		while(isActive()==true){
		try{
			System.out.println("Robotul "+rname+" se afla pe ("+x+","+y+")");
			int direction=(int)(Math.random()*100)%4;
			switch (direction){
			case 0:
				if(x<99)
				{old_coord=x;
					x++;
					if(! checkIfFree(old_coord, y, x, y))
					{
					matrix[x][y]=matrix[old_coord][y];
					matrix[old_coord][y]=null;
					}
					else
					{System.out.println("-----------");
						System.out.println("Ambuteiaj, "+matrix[x][y].toString()+" si " + matrix[old_coord][y].toString());
						matrix[x][y].setActive(false);
						matrix[old_coord][y].setActive(false);
					}
				}
				break;
			case 1:
				if(y<99) {
					old_coord=y;
					y++;
					if(! checkIfFree(x, old_coord, x, y))
					{
						matrix[x][y]=matrix[x][old_coord];
						matrix[x][old_coord]=null;
					}
					else
					{System.out.println("-----------");
						System.out.println("Ambuteiaj, "+matrix[x][y].toString()+" si " + matrix[x][old_coord].toString());
						matrix[x][y].setActive(false);
						matrix[x][old_coord].setActive(false);
					}
				}break;
			case 2:
				if(x>1){
				old_coord=x;
					x--;
					if(! checkIfFree(old_coord, y, x, y))
					{
						matrix[x][y]=matrix[old_coord][y];
						matrix[old_coord][y]=null;
					}
					else
					{
						System.out.println("-----------");
						System.out.println("Ambuteiaj, "+matrix[x][y].toString()+" si " + matrix[old_coord][x].toString());
						matrix[x][y].setActive(false);
						matrix[old_coord][y].setActive(false);
					}
				}
				break;
			case 3:
				if(y>1){
					old_coord=y;
					y--;
					if(! checkIfFree(x, old_coord, x, y))
					{
						matrix[x][y]=matrix[x][old_coord];
						matrix[x][old_coord]=null;
					}
					else
					{System.out.println("-----------");
						System.out.println("Ambuteiaj, "+matrix[x][y].toString()+" si " + matrix[x][old_coord].toString());
						matrix[x][y].setActive(false);
						matrix[x][old_coord].setActive(false);
					}}
				break;

			}

			Thread.sleep(100);

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		if( !isActive())
			return;
	}
	}
	public boolean checkIfFree(int x1,int y1, int x2,int y2)
	{
		if(matrix[x1][y1]!=null && matrix[x2][y2]!=null)
			return true;
		else
			return false;


	}
	public boolean isActive() {
		return active;
	}

	public boolean  checkValid(int x, int y)
	{
		if(matrix[x][y]==null)
			return true;
		else
			return false;
	}
	public static void main(String [] args) 
	{

		new Robots("1.ted");
		new Robots("2.darius");
		new Robots("3.simi");
		new Robots("4.marius");
		new Robots("5.passat");
		new Robots("6.ionut");
		new Robots("7.stefan");
		new Robots("8.cata");
		new Robots("9.sergiu");
		new Robots("10.iuli");



	}
	

}
