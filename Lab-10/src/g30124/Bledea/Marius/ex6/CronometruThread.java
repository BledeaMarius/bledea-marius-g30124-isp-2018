package g30124.Bledea.Marius.ex6;

class CronometruThread extends Thread {
    Cronometru cronometru;
    int nr=0;
    public void run()
    {
        while(cronometru.isActive()==true)
        {
            try{
                Thread.sleep(1000);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
            synchronized (cronometru){
            nr++;
            cronometru.setTimerValue(nr);
            }

        }
    }

}