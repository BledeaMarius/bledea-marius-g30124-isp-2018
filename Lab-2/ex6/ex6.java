package g30124.Bledea.Marius.L2.ex6;
import java.util.Scanner;
public class ex6 {
    public static void main(String[] args) {
        int n,fact;
        Scanner input=new Scanner(System.in);

        System.out.println("Enter n");
        n=input.nextInt();
        nonFactorial(n);
        fact=factorialMethod(n);
        System.out.println("Factorial method "+n+"!="+fact);
    }
    static void nonFactorial ( int n)
    {
        int i,fact=1;
        for(i=1;i<=n;i++)
            fact*=i;
        System.out.println("Non Factorial method "+n+"!="+fact);
    }
    static int factorialMethod(int nr)
    {
        if (nr==1)
            return 1;
        else return nr*factorialMethod(nr-1);
    }

}
