package g30124.Bledea.Marius.L2.e3;
import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {
        int A,B;
        Scanner input=new Scanner(System.in);

        System.out.println("A=");

        A=input.nextInt();
        System.out.println("B=");

        B=input.nextInt();
        System.out.println(isPrime(4));
        ShowPrimes(A,B);

    }
    static void ShowPrimes(int a,int b)
    {
        int contor,nr_prime=0;
        for(contor=a+1;contor<b;contor++)
        {
            if(isPrime(contor)==1) {
                nr_prime++;
                System.out.println(contor);
            }

        }
        System.out.println("There are "+nr_prime+" prime numbers");
    }
    static int isPrime(int n)
    {
        int i,ok=1;
        for(i=2;i<=Math.sqrt(n) && ok==1;i++)
            if(n%i==0)
                ok=0;

       if (ok==1)
           return 1;
       else
           return 0;
    }

}
