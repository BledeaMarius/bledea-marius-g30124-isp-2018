package g30124.Bledea.Marius.L7.ex3;

import g30124.Bledea.Marius.L7.ex2.Bank;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Main {
static void displayAll(Set list)
{
    Iterator i=list.iterator();
    while(i.hasNext())
    {
        String s=(String)i.next();
        System.out.println(s);
    }
}
    public static void main(String[] args) {
        Bank bank=new Bank();
        bank.addAccount("Sergiu",200.2);
        bank.addAccount("Marius",1201);
        bank.addAccount("Louie",40);
        bank.printAccounts();
    }
}
