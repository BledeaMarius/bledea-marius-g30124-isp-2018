package g30124.Bledea.Marius.L3.ex4;
import java.util.Scanner;
public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint point1=new MyPoint();
        MyPoint point2=new MyPoint();
        Scanner input=new Scanner(System.in);
        System.out.println("Before setting the coordinates "+point1.toString());
        int x1,y1;
        System.out.println("Enter x coordinate: ");
        x1=input.nextInt();
        System.out.println("Enter the y coordinate: ");
        y1=input.nextInt();

        point1.setX(x1);
        point1.setY(y1);
        System.out.println("Enter the coordinates for the second point : x=.. y=..");

        x1=input.nextInt();
        y1=input.nextInt();

        point2.setX(x1);
        point2.setY(y1);

        System.out.println("After setting the first (myPoint) point "+point1.toString());
        System.out.println("After setting the second point "+point2.toString());
        System.out.println("Enter the distance to calculate x=   y=");
        x1=input.nextInt();
        y1=input.nextInt();
        System.out.println("Distance between ("+x1+","+y1+") and "+point1.toString()+" = "+ point1.distance(x1,y1));
        System.out.println("Distance between "+point1.toString()+" and "+point2.toString()+"="+point1.distance(point2));
    }
}
