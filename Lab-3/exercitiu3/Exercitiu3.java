package g30124.Bledea.Marius.L3.exercitiu3;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;

public class Exercitiu3 {
    public static void main(String[] args) {
        //intial situation (1,1)
        City cluj=new City();
        Robot john= new Robot(cluj,1,1, Direction.EAST);
        int i;
        for(i=1;i<=5;i++)
            john.move(); // going 5 times
        for(i=1;i<=2;i++)  // turns around
        john.turnLeft();
        for(i=1;i<=5;i++)
            john.move();

    }

}
