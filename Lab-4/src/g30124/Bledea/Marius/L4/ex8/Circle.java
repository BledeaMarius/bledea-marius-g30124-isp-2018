package g30124.Bledea.Marius.L4.ex8;

import static java.lang.Math.round;

public class Circle extends Shape {
    private double radius=1.0;

    public Circle()
    {
        radius=1.0;
    }
    public Circle(double radius)
    {
        this.radius=radius;
    }


    public Circle(double radius,String color,boolean filled)
    {
        super(color,filled);
        this.radius=radius;

    }

    public double getRadius(
    )
    {
        return this.radius;
    }
    public void setRadius(double radius)
    {
    this.radius=radius;
    }
    public double getArea()
    {
        return round((Math.PI*Math.pow(radius,2))*100.0)/100.0;
    }
    public double getPerimeter()
    {
        return 2*Math.PI*radius;
    }

    @Override
    public String toString() {
        return "A Circle with radius="+radius+" which is" +
                " sublass of "+ super.toString();
    }
}
