package g30124.Bledea.Marius.L4.ex4;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class AuthorTest {
    private Author author;
    @Before
    public void setUp(){
        author=new Author("Marius Bledea","bledeam@yahoo.com",'m');

    }
    @Test
    public void setEmailTest()
    {
        author.setEmail("dariussabad@gmail.com");
        assertEquals(author.getEmail(),"dariussabad@gmail.com");
    }
    @Test
    public void getGenderTest()
    {
        assertEquals(author.getGender(),'m');
    }
    @Test
    public void getNameTest()
    {
        assertEquals(author.getName(),"Marius Bledea");
    }
    @Test
    public void stringMethod()
    {
        Author a3=new Author("Leahu","stefanel@gmail.com",'m');
        assertEquals(a3.toString(),"Author Leahu (m) at stefanel@gmail.com");
    }
}
