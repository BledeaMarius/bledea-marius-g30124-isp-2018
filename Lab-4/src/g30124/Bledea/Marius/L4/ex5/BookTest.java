package g30124.Bledea.Marius.L4.ex5;

import g30124.Bledea.Marius.L4.ex4.Author;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;


public class BookTest {
    Book book;
    Author author;
    @Before
    public void setUp()
    { author=new Author("marius","marius@yahoo.com",'m');
        book=new Book("bledea",author,3.4);
    }
    @Test
    public void testName()
    {
        assertEquals(book.getAuthor(),"Colt alb");

    }
    @Test
    public void getQty()
    {
        book.setQtyInStock(12);
        assertEquals(book.getQtyInStock(),12);

    }
    @Test
    public void shouldPrintInfo()
    {
        assertEquals("Book name=bledea Author marius (m) at marius@yahoo.com",book.toString());
    }
}
