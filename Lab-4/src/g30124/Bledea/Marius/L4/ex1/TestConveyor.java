package g30124.Bledea.Marius.L4.ex1;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestConveyor {
    private Conveyor c;
     @Before
     public void setUp()
     {
         c=new Conveyor();
     }
    @Test
    public void shouldAddBox(){
        //Conveyor c = new Conveyor();
        Box b = new Box(c,0,1);
        assertEquals(b.getId(), c.getBox(0).getId());
    }

    @Test
    public void shouldMoveBoxToRight(){
        //Conveyor c = new Conveyor();
        Box b = new Box(c,1,1);
        c.moveRight();
        assertEquals(b.getId(), c.getBox(2).getId());
    }

    @Test
    public void shouldMoveBoxToLeft(){
        //Conveyor c = new Conveyor();
        Box b = new Box(c,1,1);
        c.moveLeft();
        assertEquals(b.getId(),c.getBox(0).getId()); //trebuie pornit de la index 0

    }
      @Test
    public void shouldPickBox()
      {
          Box b=new Box(c,2,8);

          assertEquals(c.pickBox(2).getId(),b.getId());
          assertEquals(null,c.getBox(2));

      }

}