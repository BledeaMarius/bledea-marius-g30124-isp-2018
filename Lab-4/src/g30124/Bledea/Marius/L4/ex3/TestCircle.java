package g30124.Bledea.Marius.L4.ex3;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestCircle {
    private Circle c1;
    @Before
    public void setUp()
    {
        c1= new Circle(4);
    }

    @Test
    public void getRadius() {
        assertEquals(c1.getRadius(),4);
    }

    @Test
    public void getArea() {
        assertEquals(c1.getArea(),25.13);

    }
}
