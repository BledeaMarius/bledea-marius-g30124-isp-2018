package g30124.Bledea.Marius.L4.ex3;

public class Circle {
    private double radius=1.0;
    private String color="red";

    public Circle ( double r)
    {
        radius=r; }

    public  Circle() {
        radius=1.0;
    }

    public double getRadius()
    {return radius;
    }

    public double getArea()
    {
        return Math.round(Math.PI * 2 * radius*100.0)/100.0;//In order to display only 2 decimals
    }
}