package g30124.Bledea.Marius.L4.ex6;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

import g30124.Bledea.Marius.L4.ex4.Author;
public class TestBook {
    private Author a1;
    private Author a2;
    private Author[] a;
    private Book b1;
    @Before
    public void setUp()
    {    a1=new Author("Marius Bledea","bledea@yahoo.com",'m');
         a2=new Author("Leahu Stefanel","leahu05@gmail.com",'f');
         a=new Author[]{a1,a2};
         b1=new Book ("Colt alb",a,45.2);
    }
    @Test
    public void shouldGetName()
    {
        assertEquals(b1.getName(),"Colt alb");
    }
    @Test
    public void shouldGetPrice()
    { assertEquals(b1.getPrice(),45.2);
        b1.setPrice(40);
        assertEquals(b1.getPrice(),40.0);
    }
    @Test
    public void shouldPrintAuthors(){
        b1.printAuthors();
    }
}
