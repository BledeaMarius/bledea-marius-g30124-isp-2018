package g30124.Bledea.Marius.L4.ex6;
import g30124.Bledea.Marius.L4.ex4.Author;

import java.util.Arrays;

public class Book {
    private String name;
    private Author[] author;
    private double price;
    private int qtyInStock=0;

    public Book(String name, Author[] author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Book(String name, Author[] author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }


    public void setPrice(double price) {
        this.price = price;
    }

    public String toString() {
        return "Book-name" + getName() + '\'' +
                " by " + author.length;
    }

    public void printAuthors()
    {
        for(Author a: this.getAuthors())
            System.out.println(a);
    }
}
