package g30124.Bledea.Marius.L5.ex1;



public class TestShapes {
    public static void main(String[] args) {


        Shape[] shapes = new Shape[3];
        shapes[0] = new Circle(4);
        shapes[1] = new Rectangle(2, 2);
        shapes[2] = new Square(5);

        for (int i = 0; i<shapes.length; i++)

            System.out.println( shapes[i].getArea()+"\n"+shapes[i].getPerimeter());


    }
}
