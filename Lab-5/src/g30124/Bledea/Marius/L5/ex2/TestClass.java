package g30124.Bledea.Marius.L5.ex2;



public class TestClass {


    public static void main(String[] args) {
        ProxyImage proxyImage=new ProxyImage("pic.jpg",false);
        ProxyImage proxyImage1=new ProxyImage("picture2.png",true);
        proxyImage.display();
        System.out.println("----");
        proxyImage1.display();
    }

}
