package g30124.Bledea.Marius.L5.ex3;

import java.util.Random;

public class LightSensor extends Sensor {
    public  int lightSensor;
    @Override
    public int readValue() {
        Random number=new Random();
        lightSensor=number.nextInt(100);
        return lightSensor;
    }
}
